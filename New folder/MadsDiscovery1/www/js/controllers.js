angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  connectDB();
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('AddEventCtrl', function($scope, $ionicModal, $cordovaCamera) {
  // var today = new Date();
  // var dateout = today.getFullYear() + "/" today.getMonth() + 1 + "/" + today.getDate();
  //   var timeStart = today.getHours() + ":" + today.getSeconds();
  $scope.eventData = {};
  $scope.eventData.EvtDate = new Date();
  $scope.eventData.EvtTime = new Date();
  $scope.eventData.EvtLocaTitle = "";
  $scope.eventData.EvtName = "";
  $scope.eventData.EvtOrganizer = "";


  $scope.takePicture = function() {
    var options = {
      quality : 75,
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : Camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.eventData.FeaImgUrl = "data:image/jpeg;base64," + imageData;
      console.log("anh url :"+imageData);
    }, function(err) {
      $scope.eventData.FeaImgUrl = "img/ionic.png";
    });
  }

  $scope.doAddEvent = function() {
    var dateinput = $scope.eventData.EvtDate.getMonth() + 1 + "/" +$scope.eventData.EvtDate.getDate() + "/" + $scope.eventData.EvtDate.getFullYear();
    var timeinput = $scope.eventData.EvtTime.getHours() + ":" + $scope.eventData.EvtTime.getSeconds();
    insertEvent($scope.eventData.EvtName, $scope.eventData.EvtLocaTitle, dateinput, timeinput, $scope.eventData.EvtOrganizer, $scope.eventData.FeaImgUrl);
  };
})

.controller('PlaylistsCtrl', function($scope,$ionicPopup,$ionicModal) {
  getListEvent($scope);
  $scope.delete = function(eventId){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Deete Event',
      template: 'Are you sure you want to delete this event?',
    });

    confirmPopup.then(function(res) {
      if(res) {
        deleteEvent(eventId);
        console.log('You are sure');
      } else {
        console.log('You are not sure');
      }
    });

    // deleteEvent(eventId);
  };
})

.controller('PlaylistCtrl', function($scope, $stateParams,$cordovaCamera) {
  var idevent = $stateParams.playlistId;
  getOneEvent($scope, idevent);
  $scope.takePicture = function() {
    var options = {
      quality : 75,
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : Camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.event.FeaImgUrl = "data:image/jpeg;base64," + imageData;
      console.log("anh url :"+$scope.event.FeaImgUrl);
    }, function(err) {
      $scope.event.FeaImgUrl = "img/ionic.png";
    });
  }

    $scope.edit = function() {
      editEvent($scope, idevent);
    };
  })

  .controller('CreateReport', function($scope) {
    $scope.report = {};
    $scope.report.RepShortContent = "";
    $scope.report.RepShortContent = "";
    $scope.report.RepShortContent = "";
    $scope.report.RepContent = "";
  })
  ;

  function validation(name, loc, date, time, organizer) {
    var timeFormat = /^([0-9]{2})\:([0-9]{2})$/;
    var dateFormat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    name = typeof name !== 'undefined' ? name : '';
    loc = typeof loc !== 'undefined' ? loc : '';
    date = typeof date !== 'undefined' ? date : '';
    time = typeof time !== 'undefined' ? time : '';
    organizer = typeof organizer !== 'undefined' ? organizer : '';
    var dateinput = $scope.eventData.EvtDate.getFullYear() + "-" + $scope.eventData.EvtDate.getMonth() + 1 + "-" + $scope.eventData.EvtDate.getDate();
    var timeinput = $scope.eventData.EvtTime.getHours() + ":" + $scope.eventData.EvtTime.getSeconds();
    var txtError = '';
    if (name.trim() === '') {
      txtError = " - Name field is required";
    } else if (loc.trim() === '') {
      txtError = " - loaction field is required";
    } else if (organizer.trim() === '') {
      txtError = " - Organizer field is required";
    } else if (!timeinput.test(time)) {
      txtError = " - Time format is not correct </br>(Eg: 15:20)";
    } else if (!dateinput.test(date)) {
      txtError = " - Date format is not correct </br>(Eg: 1/1/2016)";
    }
    return txtError;
  };
