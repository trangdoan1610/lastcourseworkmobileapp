var db = null;

function connectDB(){
  db = window.openDatabase("MadDiscovery", 1.0, "Mad Discovery", 2000000);
  // var url = window.location.href;
    if (db != null) {
    //   dropTbEvent();
      createTable();
        console.log("CONNECT DATABASE OK");
    }
}

function createTable(){
  db.transaction(function (tx) {
    tx.executeSql("CREATE TABLE IF NOT EXISTS Event(EvtID INTEGER PRIMARY KEY AUTOINCREMENT, EvtName, EvtLocaTitle, EvtDate, EvtTime, EvtOrganizer, FeaImgUrl);");
    tx.executeSql("CREATE TABLE IF NOT EXISTS Report(RepID INTEGER PRIMARY KEY AUTOINCREMENT, EventID, RepContent, RepShortContent );");
    tx.executeSql("CREATE TABLE IF NOT EXISTS ReportImg(FeaID INTEGER PRIMARY KEY AUTOINCREMENT, ReportID, ImgUrl) ");
  },function(error){
    console.log("CREATE TABLE ERROR: "+ error.message);
  },function(){
    console.log("CREATE TABLE SUCCESS.");
  });
}

function insertEvent(EvtName, EvtLocaTitle, EvtDate, EvtTime, EvtOrganizer, FeaImgUrl){
//   var check = CheckEvent(EvtName, EvtLocaTitle,EvtDate,EvtTime, EvtOrganizer,$scope);
//   if(check === ""){
      db.transaction(function (tx){
            tx.executeSql("INSERT INTO Event(EvtName, EvtLocaTitle, EvtDate, EvtTime, EvtOrganizer, FeaImgUrl) VALUES (?,?,?,?,?,?);",[EvtName, EvtLocaTitle, EvtDate, EvtTime, EvtOrganizer, FeaImgUrl]);
        window.location = "index.html";
        },function(error){
            console.log("INSERT INTO TABLE Event ERROR: "+ error.message);
        },function(){
            console.log("INSERT INTO TABLE Event ON SUCCESS.");
        });
//   }
}

function insertReport(EventID, RepContent, RepShortContent){
  db.transaction(function (tx){
    tx.executeSql("INSERT INTO Report(EventID, RepContent, RepShortContent) VALUES (?,?,?);",[EventID, RepContent, RepShortContent]),
    window.location = "index.html";
  },function(error){
    console.log("INSERT INTO TABLE Report ERROR: "+ error.message);
  },function(){
    console.log("INSERT INTO TABLE Report ON SUCCESS.");
  });
}

function insertReportImg(ReportID, ImgUrl){
  db.transaction(function (tx){
    tx.executeSql("INSERT INTO ReportImg(ReportID, ImgUrl) VALUES (?,?);",[ReportID, ImgUrl]),
    window.location = "index.html";
  },function(error){
    console.log("INSERT INTO TABLE Report Image ERROR: "+ error.message);
  },function(){
    console.log("INSERT INTO TABLE Report Image ON SUCCESS.");
  });
}

function deleteEvent(eventID) {
    db.transaction(function (tx) {
        tx.executeSql("DELETE FROM Event WHERE EvtID = ?", [eventID]);
        window.location = "index.html";
    }, function (error) {
        console.log("DELETE EVENT ERROR " + error.message);
    }, function () {
        console.log("DELETE EVENT SUCCESS");
    });
}


function editEvent($scope, idevent) {
    console.log("Edit Data: "+ idevent);
    db.transaction(function (tx) {
        tx.executeSql("UPDATE Event SET EvtName = ?, EvtLocaTitle = ?,  EvtDate = ?, EvtTime = ?, EvtOrganizer = ?, FeaImgUrl = ? WHERE EvtID = ?",
                [$scope.event.EvtName, $scope.event.EvtLocaTitle, $scope.event.EvtDate, $scope.event.EvtTime, $scope.event.EvtOrganizer, $scope.event.FeaImgUrl, idevent]);
        window.location = "index.html";
    }, function (error) {
        console.log("EDIT EVENT ERROR " + error.message);
    }, function () {
        console.log("EDIT EVENT SUCCESS");
    });
}

function dropTbEvent() {
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE Event");
        tx.executeSql("DROP TABLE Report");
        tx.executeSql("DROP TABLE ReportImg");
    }, function (error) {
        console.log("DROP TABLE ERROR " + error.message);
    }, function () {
        console.log("DROP TABLE SUCCESS");
    });
}

function getListEvent($scope){
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM Event ORDER BY EvtID DESC;", [], function (tx, rs) {
      var numberOfEvent = rs.rows.length;
      var listEvents = [];
      for (var i = 0; i < numberOfEvent; i++) {
        var obj = {EvtID: rs.rows.item(i).EvtID,
             EvtName: rs.rows.item(i).EvtName,
             EvtLocaTitle: rs.rows.item(i).EvtLocaTitle,
             EvtDate: rs.rows.item(i).EvtDate,
             EvtTime: rs.rows.item(i).EvtTime,
             EvtOrganizer: rs.rows.item(i).EvtOrganizer,
             FeaImgUrl: rs.rows.item(i).FeaImgUrl,
            };
        listEvents.push(obj);
      }
      $scope.playlists =  listEvents;
    });
  }, function (error) {
    console.log("GET LIST EVENT ERROR " + error.message);
  });
}

  function getOneEvent($scope, idEvent){
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM Event WHERE EvtID = ?;", [idEvent], function (tx, rs) {
      var e = {"EvtName": rs.rows.item(0).EvtName,
             "EvtLocaTitle": rs.rows.item(0).EvtLocaTitle,
             "EvtDate": new Date(rs.rows.item(0).EvtDate),
             "EvtTime": new Date(rs.rows.item(0).EvtTime),
             "EvtOrganizer": rs.rows.item(0).EvtOrganizer,
             "FeaImgUrl": rs.rows.item(0).FeaImgUrl,};
      $scope.event =  e;
    });
  }, function (error) {
    console.log("GET ONE EVENT ERROR " + error.code);
  });
  }


function getListReport($scope, idEvent){
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM Report WHERE EventID = ?;", [idEvent], function (tx, rs) {
      var numberOfEvent = rs.rows.length;
      var listReports = [];
      for (var i = 0; i < numberOfEvent; i++) {
        var obj = {RepID: rs.rows.item(i).RepID,
             EventID: rs.rows.item(i).EventID,
             RepContent: rs.rows.item(i).RepContent,
             RepShortContent: rs.rows.item(i).RepShortContent,
            };
        listReports.push(obj);
      }
      $scope.listReport =  listReports;
    });
  }, function (error) {
    console.log("GET LIST REPORT ERROR " + error.message);
  });
}

// function CheckEvent(EvtName, EvtLocaTitle, EvtDate, EvtTime, EvtOrganizer, $scope){
    function CheckEvent($scope){
    db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM Event WHERE EvtName = ?, EvtLocaTitle = ?, EvtDate = ?, EvtTime = ?, EvtOrganizer = ?;", [$scope.eventData.EvtName, $scope.eventData.EvtLocaTitle, $scope.eventData.EvtDate, $scope.eventData.EvtTime, $scope.eventData.EvtOrganizer,], function (tx, rs) {
      var e = rs.rows.length();
      if(e == 0){
          $scope.msg = 'This event is existed !!!';
          alert("This event is existed !!!");
      }else{
          insertEvent($scope.eventData.EvtName, $scope.eventData.EvtLocaTitle, $scope.eventData.EvtDate, $scope.eventData.EvtTime, $scope.eventData.EvtOrganizer, $scope.eventData.FeaImgUrl);
      }
    //   $scope.msg = "";
    });
  }, function (error) {
    console.log("GET check ERROR " + error.message);
  });
}
